FROM golang:alpine as build_people
ENV CGO_ENABLED 0

RUN mkdir -p /people

WORKDIR /people
COPY go.* ./
COPY app/main.go ./

RUN go build -o app main.go

FROM alpine

COPY --from=build_people /people/app /app

CMD /app
EXPOSE 8080