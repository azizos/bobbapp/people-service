
# Changelog

All notable changes to this project will be documented in this file and are made on top of the this [original](https://github.com/deBijenkorf/demo-db-service) project.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-06-26
### Added
- Dockerfile to build artifact images
- A docker-compose for local development
- A gitlab.yaml file to perform CI/CD jobs using GitLab CI

### Changed
- New line to .gitignore to avoid the local .env file to be committed
- README to include new information
  
### Removed
- N/A