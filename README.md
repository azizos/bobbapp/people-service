# People Service
This is a sample application which queries a simple database for names and serves those names from a web server.

## Technical Overview
More info here: https://gitlab.com/azizos/bobbapp/env/infra

## Run everywhere!
So there are 3 ways to run and use this service.

### Go (local)
#### _Prerequisites_ 
- `Go`
-  a running PostgreSQL server_

To run locally, you'll first need to install Go and make sure a PostgreSQL server is already running.
Then you can simply build the project using `go build -o app main.go` and run the generated executable artifact (`app`).

Default port: 8080

### docker-compose (local)
#### _Prerequisites_ 
- `docker-compose`

Run the command below in this project's root directory and the service is locally available on port 8080 (by default) :)
-`docker-compose up --build --remove-orphans`

### Kubernetes

Please find the manifest files and instructions [here](https://gitlab.com/azizos/bobbapp/env).